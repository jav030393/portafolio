"use strict";
const webpack = require("webpack");
const { VueLoaderPlugin } = require("vue-loader");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const MinifyPlugin = require("babel-minify-webpack-plugin");
const ImageminPlugin = require("imagemin-webpack");
const path = require("path");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;
const CompressionPlugin = require("compression-webpack-plugin");

function resolve(dir) {
  return path.join(__dirname, "..", dir);
}

module.exports = {
  mode: "development",
  entry: ["./src/app.js"],
  output: {
    path: resolve("dist"),
    filename: "bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: "vue-loader",
      },
      {
        test: /\.(gif|svg)$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[hash].[ext]",
              outputPath: "assets/img",
              publicPath: "assets/img",
              emitFile: true,
              esModule: false,
            },
          },
          {
            loader: ImageminPlugin.loader,
            options: {
              bail: false,
              cache: false,
              imageminOptions: {
                plugins: [
                  ["gifsicle", { interlaced: true, optimizationLevel: 3 }],
                  [
                    "svgo",
                    {
                      plugins: [{ removeViewBox: false }],
                    },
                  ],
                ],
              },
            },
          },
        ],
      },
      {
        test: /\.(jpe?g|png|jpg)$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[hash].[ext]",
              outputPath: "assets/img",
              publicPath: "assets/img",
              emitFile: true,
              esModule: false,
            },
          },
          {
            loader: "webp-loader",
            options: {
              quality: 13,
            },
          },
        ],
      },
      {
        test: /\.(pdf)$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "assets",
              publicPath: "assets",
              emitFile: true,
              esModule: false,
            },
          },
          {
            loader: "webp-loader",
            options: {
              quality: 13,
            },
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          "vue-style-loader",
          "css-loader?sourceMap",
          "sass-loader?sourceMap",
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: "vue-style-loader",
          },
          {
            loader: "css-loader?sourceMap",
          },
        ],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: "index.html",
      inject: true,
    }),
    new MiniCssExtractPlugin({
      filename: "main.css",
    }),
    new MinifyPlugin(
      {},
      {
        comments: false,
      }
    ),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new CompressionPlugin({
      algorithm: "gzip",
    }),
  ],
};
