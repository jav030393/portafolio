import Vue from "vue";
import App from "./App.vue";
import "./sass/variables.scss";
import { store } from "./store";

import router from "./router";
import i18n from "./i18n";

router.beforeEach((to, from, next) => {
  let language = store.getters.getActiveLanguage;
  if (!language) {
    language = "en";
  }

  i18n.locale = language;
  next();
});

new Vue({
  router,
  i18n,
  store,
  el: "#app",
  render: (h) => h(App),
});
