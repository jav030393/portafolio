import Vue from "vue";
import Vuex from "vuex";
import VueLocalStorage from "vue-localstorage";
import i18n from "./i18n";

Vue.use(Vuex);
Vue.use(VueLocalStorage);

export const store = new Vuex.Store({
  state: {
    profile: {
      firstName: "Javier",
      lastName: "Baró Morales",
      profession: "app.profession",
      overview: "home.overview",
      imageBody: require("./assets/img/profile-body-img.png"),
      image: require("./assets/img/profile-img.png"),
      email: "javierbaromorales@gmail.com",
      cv: require("./assets/cv_Javier_Baro.pdf"),
      values: [
        {
          icon: require("./assets/img/projects-icon.svg"),
          quantity: 10,
          text: "home.projects",
          type: "number",
        },
        {
          icon: require("./assets/img/languages-frameworks-icon.svg"),
          quantity: 8,
          text: "home.languagesFrameworks",
          type: "number",
        },
        {
          icon: require("./assets/img/experience-icon.svg"),
          quantity: 1.4,
          text: "home.experience",
          type: "year",
        },
      ],
      technologyChart: {
        labels: [
          "JavaScript",
          "Html",
          "Git",
          "laravel",
          "Sass",
          "Css",
          "Java",
          "php",
          "SQL",
          "Vue",
          "Spring Boot",
          "Symfony",
        ],
        images: [
          require("./assets/img/js.png"),
          require("./assets/img/html.png"),
          require("./assets/img/git.png"),
          require("./assets/img/laravel.png"),
          require("./assets/img/sass.png"),
          require("./assets/img/css.png"),
          require("./assets/img/java.png"),
          require("./assets/img/php.png"),
          require("./assets/img/sql.png"),
          require("./assets/img/vue.png"),
          require("./assets/img/spring-boot.png"),
          require("./assets/img/symfony.png"),
        ],
        datasets: [
          {
            backgroundColor: [
              "rgb(235, 219, 80)",
              "rgb(239, 179, 89)",
              "rgb(240, 81, 51)",
              "rgb(243, 80, 69)",
              "rgb(210, 110, 163)",
              "rgb(126, 41, 255)",
              "rgb(61, 81, 184)",
              "rgb(137, 147, 190)",
              "rgb(54, 162, 221)",
              "rgb(66, 184, 130)",
              "rgb(55, 180, 50)",
              "rgb(30, 30, 30)",
            ],
            data: [6, 10, 7, 3, 8, 8, 8, 7, 7, 3, 7, 6],
          },
          {
            backgroundColor: [
              "rgba(235, 219, 80, 0.5)",
              "rgba(239, 179, 89, 0.5)",
              "rgba(240, 81, 51, 0.5)",
              "rgba(243, 80, 69, 0.5)",
              "rgb(210, 110, 163, 0.5)",
              "rgba(126, 41, 255, 0.5)",
              "rgba(61, 81, 184, 0.5)",
              "rgba(137, 147, 190, 0.5)",
              "rgba(51, 153, 219, 0.5)",
              "rgba(66, 184, 130, 0.5)",
              "rgba(55, 180, 50, 0.5)",
              "rgba(30, 30, 30, 0.5)",
            ],
            data: [6, 10, 7, 3, 8, 8, 8, 7, 7, 3, 7, 6],
          },
        ],
      },
      educationTimeline: [
        {
          title: "home.latina-university",
          info: "",
          startDate: "2016",
          endDate: "2019",
        },
        {
          title: "home.ipi",
          info: "10° a 12° | CUBA",
          startDate: "2009",
          endDate: "2011",
        },
        {
          title: "home.esbu",
          info: "9° | CUBA",
          startDate: "2008",
          endDate: "2009",
        },
        {
          title: "home.cultural",
          info: "4° a 8° | PANAMÁ",
          startDate: "2003",
          endDate: "2007",
        },
      ],
      experienceTimeline: [
        {
          title: "Getecsa",
          info: "home.full-time",
          startDate: "12/2020",
          endDate: "home.current",
        },
        {
          title: "Rootstack",
          info: "home.full-time",
          startDate: "10/2019",
          endDate: "06/2020",
        },
        {
          title: "CrimsonLogic Panamá",
          info: "home.professional-practice",
          startDate: "03/2018",
          endDate: "10/2018",
        },
        {
          title: "Havanatur Celimar",
          info: "home.full-time-technician",
          startDate: "09/2012",
          endDate: "10/2015",
        },
      ],
      projects: [
        {
          img: require("./assets/img/portfolio-img.png"),
          name: "Portfolio",
          link: "https://gitlab.com/jav030393/portafolio",
        },
        {
          img: require("./assets/img/carrental.png"),
          name: "Car Rental",
          link: "https://gitlab.com/jav030393/carrental",
        },
      ],
      socialNetwork: [
        {
          icon: require("./assets/img/linkedin-icon.svg"),
          name: "LinkedIn",
          link: "https://pa.linkedin.com/in/javier-baro-morales-75183717b",
        },
        {
          icon: require("./assets/img/gitlab-icon.svg"),
          name: "gitlab",
          link: "https://gitlab.com/jav030393",
        },
      ],
    },
    navItems: [
      {
        route: "/projects",
        machineName: "projects.title",
      },
      {
        route: "/",
        machineName: "home.title",
      },
      {
        route: "/contact",
        machineName: "contact.title",
      },
    ],
    navActive: Vue.localStorage.get("activeNavItem") || 1,
    locales: Object.keys(i18n.messages),
    activeLanguage: Vue.localStorage.get("language") || i18n.locale,
  },
  getters: {
    navItems(state) {
      return state.navItems;
    },
    navActive(state) {
      return state.navActive;
    },
    locales(state) {
      return state.locales;
    },
    getActiveLanguage(state) {
      return state.activeLanguage;
    },
    getProfile(state) {
      return state.profile;
    },
  },
  mutations: {
    switchLanguage(state, locale) {
      if (state.activeLanguage !== locale) {
        Vue.localStorage.set("language", locale);
        state.activeLanguage = locale;
        i18n.locale = locale;
      }
    },
    setActiveNavItem(state, item) {
      Vue.localStorage.set("activeNavItem", item);
      state.navActive = item;
    },
  },
  actions: {
    switchLanguage(context, locale) {
      context.commit("switchLanguage", locale);
    },
    setActiveNavItem(context, item) {
      context.commit("setActiveNavItem", item);
    },
  },
});
